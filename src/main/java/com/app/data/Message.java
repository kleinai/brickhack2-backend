package com.app.data;

import com.app.KeyService;
import com.app.serializer.MessageSerializer;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import jdk.nashorn.internal.ir.annotations.Ignore;
import org.hibernate.service.spi.InjectService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author Aidan
 */
@Entity
@JsonSerialize(using = MessageSerializer.class)
public class Message {

    // ID
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    // Chat tags
    public String[] tags;

    @Transient
    public String plainText;

    // Message bytes
    @Lob
    @Column(length = 65536)
    public byte[] message;

    /**
     * The message's key
     */
    @Lob
    @Column(length = 32)
    public byte[] key;

    /**
     * The message's initialization vector
     */
    @Lob
    @Column(length = 32)
    public byte[] iv;

    public long created;

    /**
     * The user's hashed code
     */
    @Lob
    @Column(length = 32)
    public byte[] user;

    /**
     * Location
     */
    public BigDecimal lat;
    public BigDecimal lon;
    public int dist;

    protected Message() {}

    public Message(byte[] message, byte[] key, byte[] iv, String[] tags, byte[] user, BigDecimal lat, BigDecimal lon, int dist) {
        this.message = message;
        this.key = key;
        this.iv = iv;
        this.tags = tags;
        this.created = System.currentTimeMillis();
        this.user = user;
        this.lat = lat;
        this.lon = lon;
        this.dist = dist;
    }
}
