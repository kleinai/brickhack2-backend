package com.app.data;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Aidan
 */
@Repository
@Transactional
public class MessageDAOImpl implements MessageDAO {

    static final double EARTH_RADIUS = 6371000;

    private static double[] FILTER_DIST = new double[] {
            1000,
            10000,
            100000,
            1000000
    };

    private static Logger LOGGER = Logger.getLogger(MessageDAOImpl.class);

    @PersistenceContext
    private EntityManager manager;

    public List<Message> getAllMessagesByTag(String tag) {
        List<Message> messages = manager.createQuery("SELECT m FROM Message m", Message.class).getResultList();
        messages = messages.stream().filter(m ->
                Arrays.stream(m.tags).anyMatch(t -> {
                    LOGGER.info(t);
                    return t.equalsIgnoreCase(tag);
                }
            )
        ).collect(Collectors.toList());
        return messages;
    }

    public List<Message> getAllMessagesWithinLocation(BigDecimal lat, BigDecimal lon, int dist) {
        List<Message> messages = manager.createQuery("SELECT m FROM Message m", Message.class).getResultList();

        // Convert source latitude to radians
        final double srcLatRadians = Math.toRadians(lat.doubleValue());

        messages = messages.parallelStream().filter(m -> {
            // Convert destinaton coordinates to radians
            double destLatRadians = Math.toRadians(m.lat.doubleValue());
            double deltaLatRadians = Math.toRadians(lat.subtract(m.lat).doubleValue());
            double deltaLonRadians = Math.toRadians(lon.subtract(m.lon).doubleValue());

            double a = Math.pow(Math.sin(deltaLatRadians/2.0), 2) +
                        Math.cos(srcLatRadians) * Math.cos(destLatRadians) * Math.pow(Math.sin(deltaLonRadians/2.0), 2);

            double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

            double d = EARTH_RADIUS * c;

            if (d < FILTER_DIST[dist] && d < FILTER_DIST[m.dist]) {
                return true;
            }
            return false;
        }).collect(Collectors.toList());

        return messages;
    }

    public boolean save(Message message) {
        try {
            manager.persist(message);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public void clean() {
        // Delete all messages
        manager.createQuery("SELECT m FROM Message m", Message.class).getResultList().stream().map(m -> {
            manager.remove(m);
            return true;
        });
    }
}
