package com.app.data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author Aidan
 */
public interface MessageDAO {

    List<Message> getAllMessagesByTag(String tag);
    List<Message> getAllMessagesWithinLocation(BigDecimal lat, BigDecimal lon, int dist);

    boolean save(Message message);

    void clean();
}
