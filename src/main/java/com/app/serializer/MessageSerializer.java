package com.app.serializer;

import com.app.data.Message;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.springframework.util.Base64Utils;

import java.io.IOException;
import java.util.Arrays;

/**
 * @author Aidan
 */
public class MessageSerializer extends JsonSerializer<Message> {
    @Override
    public void serialize(Message value, JsonGenerator gen, SerializerProvider serializers) throws IOException, JsonProcessingException {
        gen.writeStartObject();
        gen.writeFieldName("id");
        gen.writeNumber(value.id);
        gen.writeFieldName("message");
        gen.writeString(value.plainText);
        gen.writeFieldName("time");
        gen.writeNumber(value.created);
        gen.writeFieldName("owner");
        gen.writeString(Base64Utils.encodeToString(value.user));
        gen.writeFieldName("tags");
        gen.writeStartArray();
        for(String t : value.tags) {
            gen.writeString(t);
        }
        gen.writeEndArray();
        gen.writeEndObject();
    }
}
