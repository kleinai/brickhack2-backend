package com.app;

import com.app.data.Message;
import com.app.data.MessageDAO;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Base64Utils;
import org.springframework.web.bind.annotation.*;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Aidan
 */
@RestController
public class MessageController {

    private static Logger LOGGER = Logger.getLogger(MessageController.class);
    private static ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    KeyService keyService;

    @Autowired
    MessageDAO messageDAO;

    /**
     * Returns a list of messages
     * @param body - Request body
     * @return List of messages if successful. Error if otherwise
     */
    @RequestMapping(path = "/messages/fetch", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity fetchMessage(@RequestBody Map<String, String> body) {
        if (!body.containsKey("lat") || !body.containsKey("lon") || !body.containsKey("dist")) {
            ObjectNode retNode = objectMapper.createObjectNode();
            retNode.put("status", "error");
            retNode.put("message", "A location must be provided");
            return new ResponseEntity<>(retNode, HttpStatus.BAD_REQUEST);
        }

        BigDecimal lat = new BigDecimal(body.get("lat"));
        BigDecimal lon = new BigDecimal(body.get("lon"));
        int dist = new Integer(body.get("dist"));

        List<Message> messages = messageDAO.getAllMessagesWithinLocation(lat, lon, dist);

        if (body.containsKey("tag")) {
            messages = messages.stream().filter(m ->
                    Arrays.stream(m.tags).anyMatch(t ->
                            t.equalsIgnoreCase(body.get("tag")))
            ).collect(Collectors.toList());
        } else {
            messages = messages.stream().filter(m ->
                m.tags.length == 0
            ).collect(Collectors.toList());
        }

        if (messages.size() > 0) {
            KeyPair keyPair = keyService.getActiveKeyPair();

            try {
                // Create the RSA cipher
                Cipher cipherRSA = null;
                cipherRSA = Cipher.getInstance("RSA/ECB/PKCS1Padding");
                cipherRSA.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());

                // Create the AES cipher
                Cipher cipherAES = null;
                cipherAES = Cipher.getInstance("AES/CBC/PKCS5Padding");

                for(Message m : messages) {
                    SecretKeySpec keySpecAES = new SecretKeySpec(cipherRSA.doFinal(m.key), "AES");
                    cipherAES.init(Cipher.DECRYPT_MODE, keySpecAES, new IvParameterSpec(m.iv));

                    m.plainText = new String(cipherAES.doFinal(m.message), StandardCharsets.UTF_16);
                }

            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        ObjectNode retNode = objectMapper.createObjectNode();
        return new ResponseEntity<>(messages, HttpStatus.OK);
    }

    @RequestMapping(path = "/messages/send", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity sendMessage(@RequestBody Map<String, String> body) {
        // Verify fields
        if (!body.containsKey("message")) {
            ObjectNode retNode = objectMapper.createObjectNode();
            retNode.put("status", "error");
            retNode.put("message", "A message must be provided");
            return new ResponseEntity<>(retNode, HttpStatus.BAD_REQUEST);
        }

        if (!body.containsKey("user")) {
            ObjectNode retNode = objectMapper.createObjectNode();
            retNode.put("status", "error");
            retNode.put("message", "A user code must be provided");
            return new ResponseEntity<>(retNode, HttpStatus.BAD_REQUEST);
        }

        if (!body.containsKey("lat") || !body.containsKey("lon") || !body.containsKey("dist")) {
            ObjectNode retNode = objectMapper.createObjectNode();
            retNode.put("status", "error");
            retNode.put("message", "A location must be provided");
            return new ResponseEntity<>(retNode, HttpStatus.BAD_REQUEST);
        }

        BigDecimal lat = new BigDecimal(body.get("lat"));
        BigDecimal lon = new BigDecimal(body.get("lon"));
        int dist = new Integer(body.get("dist"));

        // Get the active key
        KeyPair keyPair = keyService.getActiveKeyPair();

        // User hash
        byte[] userHash = keyService.hashUser(body.get("user"));
        String userHashB64 = Base64Utils.encodeToString(userHash);

        try {
            // Create the cipher
            Cipher cipherRSA = Cipher.getInstance("RSA/ECB/PKCS1Padding");
            cipherRSA.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());

            SecureRandom random = new SecureRandom();
            byte[] messageKey = new byte[32];
            random.nextBytes(messageKey);

            byte[] iv = new byte[16];
            random.nextBytes(iv);

            SecretKeySpec secretKeySpec = new SecretKeySpec(messageKey, "AES");

            // Create the cipher
            Cipher cipherAES = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipherAES.init(Cipher.ENCRYPT_MODE, secretKeySpec, new IvParameterSpec(iv));

            // Encrypt the message
            byte[] cipherMessage = cipherAES.doFinal(body.get("message").getBytes(StandardCharsets.UTF_16));

            // Encrypt the message key
            byte[] cipherKey = cipherRSA.doFinal(messageKey);

            // Save the message
            Message message = new Message(cipherMessage, cipherKey, iv, body.get("tags").split(","),
                    userHash, lat, lon, dist);
            if (!messageDAO.save(message)) {
                return new ResponseEntity<>(objectMapper.createObjectNode(), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(objectMapper.createObjectNode(),HttpStatus.INTERNAL_SERVER_ERROR);
        }

        ObjectNode retNode = objectMapper.createObjectNode();
        retNode.put("status", "success");
        retNode.put("hash", userHashB64);
        return new ResponseEntity<>(retNode, HttpStatus.OK);
    }
}
