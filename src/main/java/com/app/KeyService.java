package com.app;

import com.app.data.MessageDAO;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.jce.provider.JCEKeyGenerator;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

/**
 * @author Aidan
 */
@Component
public class KeyService {

    private static Logger LOGGER = Logger.getLogger(KeyService.class);

    public static final byte[] USER_HASH_KEY = Base64Utils.decodeFromString(
            "4M89U2flTBdx2DI3ffxPT3zDgkSe1v+GiaFtLw+Frjb6b3CweZbZMPT7tR0YUxIqVc+RzoB8EARs" +
            "v7sysgOjKL+6PWB3oxS0xp3T+Q1ECaPHfgcikrlWsL13tfxkh/EXPwY5zI7Enh22Zh2BAbNTFgi/" +
            "hkD7Q0pDph3EmYDRN9O6mkkP8RceoXPxsn20659KSU4H6JdeSEQLtDTqrtgsxQVYzu+DmspLy6D1" +
            "Oir4pbgq5f5Iq9VwSrwgWZdHMDNcoQEnWuMvqUPlNSDgebzvRWBQ78wrfGM1okLypXA1nsk8hrPE" +
            "lqYf7EpRopd3vAQO7JV65OmRSiZushFjWFrK7wgTasY34MXanr4YwiTatIyQpgKYJEGV3fuDdbWg" +
            "rYibHkr3gq/Mt+O71JS6cbeoSFJJPNJyrSBryn0iN/nFm3lEQNDXETJXCgKMbnM8RV+ZipcUPkhK" +
            "LXz4bQ/6oI2zHYVsRJGPXULIk3MKRLH8siUsfHgs//CxIyVzOgX5YTnRWsXpZfPadrLmxx8kO9qn" +
            "z3dJnHOBmmphBk70BuyN33orRiU6PttCDdfGCLR0YYBjr2ito9JmdlrfvRYF8UCXyZigvUQD7Mq4" +
            "tOmQ0VJOH4llS/rAwHYSXEzmJy+Cu+HxgPGtfrSCzFdOc6Osm0ZEzRE7KTZIjkU0dilkdoCihW7K" +
            "2r2PMnl999UDaN4jW1794f9KDcpdpHuIxl7Hk0PR0I3G/z9x1Gyxrn/OlRRTUBCo+fEAitweXS8F" +
            "Fm6CpcFa9fbRiuZrR6peUkCRYOt3vx1C67P0D6D9n4/8r2i/P0/Wk6zQS+26qCyZ3QG4/zDxgLNu" +
            "Qtzqh8NgJXIskNlFEY3A7sCaZGhmjzBAlVo8usXKEOSgCZjpPCoHxN5dcOvi4djl1I7AQT2EIF2N" +
            "RoRIq9dx4huxpM6eSTWORSXFSSlTq55o0d0RVqLCWGbeHoKCHjQLSu34469v+OQC7gyQsHGYb+4r" +
            "rOFM5eJwP0bwR4KgivNKf1LSSj/BjgLe95Q1s8/s5/UnmwGJweT9E+ddooDxl1VD/yeTBYXSLcY+" +
            "iwJTB1zoM0cxAQaHkFssJ6lHQk5F08hK0Yg3pFJ0DSMjnWAJAHrh3/MeXwr0g+B3CQrhdUlkw10J" +
            "6NRwWy/6CLsICnKmVRfDqI08g7xvMp630pM3bZCDUIrZ98CmLXire2l1DDpTgE7wDF55xpjhFO/Z" +
            "DRrELdbtRaCMssnQIupYJ2ctM/kO9hSc5uCl+l00x8ed30q/3Qr28148keEMxoWonC/9Ll1CavjD" +
            "EZozLAhqa4CyZHXqGFfCE36dwFnf8/2Dup7bMhV+dt8wBpxHP7B0S98LTM4bm+qq8mqnZcFXmQ==");

    @Autowired
    MessageDAO messageDAO;

    /**
     * Key TTL
     * Length of time the key is alive for
     */
    public static long KEY_TTL = 3600*60*60;

    private static KeyPair ACTIVE_KEY;
    private static long GENERATION_TIME;

    /**
     * Updates and returns the active key
     * @return The currently active key. If this is null something is wrong.
     */
    public KeyPair getActiveKeyPair() {
        updateKey();
        return ACTIVE_KEY;
    }

    /**
     * Generates a user hash from a string
     * @param user User password
     * @return User's hash or null if error
     */
    public byte[] hashUser(String user) {
        try {
            SecretKeySpec key = new SecretKeySpec(USER_HASH_KEY, "HmacSHA1");

            Mac mac = Mac.getInstance("HmacSHA1");
            mac.init(key);

            return mac.doFinal(user.getBytes(StandardCharsets.UTF_16));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Refresh the active key
     */
    private void updateKey() {
        // Check if key isn't null or is dead
        if (ACTIVE_KEY == null || GENERATION_TIME < System.currentTimeMillis() - KEY_TTL) {
            try {
                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
                keyPairGenerator.initialize(2048);
                ACTIVE_KEY = keyPairGenerator.generateKeyPair();
                GENERATION_TIME = System.currentTimeMillis();

                messageDAO.clean();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            LOGGER.info("Regenerated key");
        }
    }
}
