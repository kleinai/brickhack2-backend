import com.app.Application;
import com.app.MessageController;
import com.app.data.MessageDAO;
import com.app.data.MessageDAOImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextBeforeModesTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Random;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Aidan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {Application.class})
@WebAppConfiguration
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class
})
@ActiveProfiles("test")
@Profile("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MessageTest {

    Logger LOGGER = Logger.getLogger(MessageTest.class);

    @Autowired
    WebApplicationContext webApplicationContext;

    MockMvc mvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    MessageDAO messageDAO;

    @Before
    public void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @Test
    public void testMessages() throws Exception {

        Random random = new Random();

        for (int i = 0; i < 50; i++) {
            ObjectNode requestNode = objectMapper.createObjectNode();
            requestNode.put("message", "Message");
            requestNode.put("tags", "tag");
            requestNode.put("tag", "tag");
            requestNode.put("user", "MyPassword");
            requestNode.put("lat", 70 + random.nextDouble());
            requestNode.put("lon", 90 + random.nextDouble());
            requestNode.put("dist", random.nextInt(4));

            mvc.perform(post("/messages/send")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(requestNode.toString()))
                    /*.andDo(r -> LOGGER.info(r.getResponse().getContentAsString()))*/
                    .andExpect(status().is2xxSuccessful());
        }

        ObjectNode requestNode = objectMapper.createObjectNode();
        requestNode.put("tag", "tag");
        requestNode.put("lat", "70");
        requestNode.put("lon", "90");
        requestNode.put("dist", 0);
        mvc.perform(post("/messages/fetch")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestNode.toString()))
                .andDo(r -> LOGGER.info(r.getResponse().getContentAsString()))
                .andExpect(status().is2xxSuccessful());
        requestNode.put("dist", 1);
        mvc.perform(post("/messages/fetch")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestNode.toString()))
                .andDo(r -> LOGGER.info(r.getResponse().getContentAsString()))
                .andExpect(status().is2xxSuccessful());
        requestNode.put("dist", 2);
        mvc.perform(post("/messages/fetch")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestNode.toString()))
                .andDo(r -> LOGGER.info(r.getResponse().getContentAsString()))
                .andExpect(status().is2xxSuccessful());
        requestNode.put("dist", 3);
        mvc.perform(post("/messages/fetch")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestNode.toString()))
                .andDo(r -> LOGGER.info(r.getResponse().getContentAsString()))
                .andExpect(status().is2xxSuccessful());
    }
}
